﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSP_PartMaintenanceTool
{
    class ModelNoDTO
    {
        public string ID { get; set; }

        public string ComboBoxItemPos { get; set; }

        public string DigitNo { get; set; }

        public string DigitValue { get; set; }

        public string DigitDescription { get; set; }

        public string DigitRepresentation { get; set; }

        public string HeatType { get; set; }

        public string ListType { get; set; }

        public string OAUTypeCode { get; set; }

        public string Revision { get; set; }
    }
}
