﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSP_PartMaintenanceTool
{
    class PartsMainDTO
    {
        public string AssemblySeq { get; set; }

        public string CriticalComp { get; set; }

        public string DateMod { get; set; }

        public string Digit { get; set; }

        public string HeatType { get; set; }

        public string ID { get; set; }

        public string LinesideBin { get; set; }

        public string PartCategory { get; set; }

        public string PartDescription { get; set; }

        public string PartNum { get; set; }

        public string PartType { get; set; }

        public string Picklist { get; set; }

        public string Qty { get; set; }

        public string RelatedOperation { get; set; }

        public string RuleBatchID { get; set; }
        
        public string StationLoc { get; set; }

        public string UnitType { get; set; }

        public string UserName { get; set; }

        public string Value { get; set; }
    }
}
