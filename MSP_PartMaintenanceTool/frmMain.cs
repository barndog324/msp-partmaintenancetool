﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSP_PartMaintenanceTool
{
    public partial class frmMain : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        PartCategoryBO objCat = new PartCategoryBO();
        ModelNoBO objModel = new ModelNoBO();

        // PartNum master fields
        public string mRuleHeadID { get; set; }
        public string mRuleBatchID { get; set; }
        public string mPartNumber { get; set; }
        public string mPartDesc { get; set; }
        public string mRelatedOp { get; set; }
        public string mPartCategory { get; set; }
        public string mUnitType { get; set; }
        public string mDigit { get; set; }
        public string mValueStr { get; set; }
        public string mQty { get; set; }
        public string mUpdatedValueStr { get; set; }
        public string mAssemblySeq { get; set; }
        public string mHeatType { get; set; }
        public string mDigitNo { get; set; }
        public string mDigitVal { get; set; }        
        public string mDigitDesc { get; set; }
        public string mDigitRep { get; set; }      
        public string mCategoryName { get; set; }
        public string mCategoryDesc { get; set; }
        public string mPullOrder { get; set; }
        public string mCritComp { get; set; }
        public string mVolts { get; set; }
        public string mModBy { get; set; }
        public string mLastModDate { get; set; }

        public frmMain()
        {           
            InitializeComponent();

#if DEBUG
            try           // If in the debug environment then set the Database connection string to the test database.
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=DEVSVR01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#endif

            //DataTable dtTest = objPart.GetJobNumberList();  // For testing database connection

            //Load PartRulesHead DataGridView
            objPart.PartNum = null;
            DataTable dt = objPart.MSP_GetPartRulesHead();
            populatePartRuleHeadList(dt);

            //Load PartConditionsDTL DataGridView
            dt = objPart.MSP_GetPartsAndRulesList();
            populatePartConditionsList(dt);
           
            //Load Model No Desc DataGridView            
            objModel.ListType = "FullList";
            dt = objModel.MSP_GetModelNoList();
            populateModelNoList(dt);
            
            //Load PartCategory DataGridView   
            objCat.PullOrder = "-1";
            dt = objCat.MSP_GetPartCategoryHead();
            populatePartCategoryDataList(dt);
            //MessageBox.Show("Read Part Cat");
        }

        #region frmMain Events
        private void btnMainExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region PartRulesHead
        private void btnPRH_Reset_Click(object sender, EventArgs e)
        {
            objPart.PartNum = null;
            txtPRH_SearchPartNum.Text = "";
            DataTable dt = objPart.MSP_GetPartRulesHead();
            populatePartRuleHeadList(dt);
        }

        private void populatePartRuleHeadList(DataTable dt)
        {
            dgvPartRulesHead.DataSource = dt;
                      
            dgvPartRulesHead.Columns["PartNum"].Width = 200;           // Part Number
            dgvPartRulesHead.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartRulesHead.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartNum"].DisplayIndex = 0;
            dgvPartRulesHead.Columns["PartDescription"].Width = 400;           // Part Description
            dgvPartRulesHead.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartRulesHead.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartDescription"].DisplayIndex = 1;
            dgvPartRulesHead.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartRulesHead.Columns["RelatedOperation"].HeaderText = "Related\nOperation";
            dgvPartRulesHead.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["RelatedOperation"].DisplayIndex = 2;
            dgvPartRulesHead.Columns["PartCategory"].Width = 150;           // PartCategory   
            dgvPartRulesHead.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartRulesHead.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartCategory"].DisplayIndex = 3;
            dgvPartRulesHead.Columns["PartType"].Width = 100;            // PartType
            dgvPartRulesHead.Columns["PartType"].HeaderText = "Part Type";
            dgvPartRulesHead.Columns["PartType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartType"].DisplayIndex = 4;
            dgvPartRulesHead.Columns["AssemblySeq"].Width = 55;            // Assembly Seq
            dgvPartRulesHead.Columns["AssemblySeq"].HeaderText = "Asm Seq";
            dgvPartRulesHead.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["AssemblySeq"].DisplayIndex = 5;
            dgvPartRulesHead.Columns["LinesideBin"].Width = 75;            // LinesideBin
            dgvPartRulesHead.Columns["LinesideBin"].HeaderText = "Lineside\nBin";
            dgvPartRulesHead.Columns["LinesideBin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["LinesideBin"].DisplayIndex = 6;
            dgvPartRulesHead.Columns["StationLocation"].Width = 75;            // StationLocation
            dgvPartRulesHead.Columns["StationLocation"].HeaderText = "Station\nLocation";
            dgvPartRulesHead.Columns["StationLocation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["StationLocation"].DisplayIndex = 7;
            dgvPartRulesHead.Columns["Picklist"].Width = 60;            // Picklist
            dgvPartRulesHead.Columns["Picklist"].HeaderText = "Picklist";
            dgvPartRulesHead.Columns["Picklist"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["Picklist"].DisplayIndex = 8;
            dgvPartRulesHead.Columns["ID"].Visible = false;
            dgvPartRulesHead.Columns["UserName"].Visible = false;  // UserName                    
            dgvPartRulesHead.Columns["DateMod"].Visible = false;   // Date Modified                      
        }
        private void dgvPartRulesHead_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum = "";            

            frmUpdatePartRulesHead frmUpd = new frmUpdatePartRulesHead(this);
            
            frmUpd.lbID.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["ID"].Value.ToString();
            partNum = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            frmUpd.txtPartNum.Text = partNum;
            frmUpd.txtPartNum.Visible = true;
            frmUpd.txtPartNum.Enabled = false;
            frmUpd.cbPRH_PartNum.Visible = false;
            frmUpd.btnReset.Enabled = false;

            //DataTable dt = objPart.GetPartNumberList();           

            //frmUpd.cbPRH_PartNum.DataSource = dt;
            //frmUpd.cbPRH_PartNum.DisplayMember = "PartNum";
            //frmUpd.cbPRH_PartNum.ValueMember = "PartDescription";
            //frmUpd.cbPRH_PartNum.Text = partNum;
            //frmUpd.cbPRH_PartNum.Enabled = false;

            frmUpd.txtPRH_PartDesc.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();

            DataTable dt = objCat.MSP_GetPartCategoryList();
            frmUpd.cbPRH_PartCategory.DataSource = dt;
            frmUpd.cbPRH_PartCategory.DisplayMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.ValueMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();

            frmUpd.cbPRH_RelatedOp.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["RelatedOperation"].Value.ToString();
            frmUpd.txtPRH_AsmSeq.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["AssemblySeq"].Value.ToString();

            if (dgvPartRulesHead.Rows[e.RowIndex].Cells["Picklist"].Value.ToString() == "Yes")
            {
                frmUpd.chkBoxPicklist.Checked = true;
            }
            else
            {
                frmUpd.chkBoxPicklist.Checked = false;
            }

            frmUpd.txtPRH_LinesideBin.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["LinesideBin"].Value.ToString();
            frmUpd.txtPRH_StationLocation.Text = dgvPartRulesHead.Rows[e.RowIndex].Cells["StationLocation"].Value.ToString();

            frmUpd.btnSave.Text = "Update";
            frmUpd.btnDelete.Enabled = true;

            frmUpd.ShowDialog();

            partNum = mPartNumber;
            objPart.PartNum = null;
            dgvPartRulesHead.DataSource = null;
            dt = objPart.MSP_GetPartRulesHead();
            populatePartRuleHeadList(dt);

            foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
            {
                if (dgr.Cells["PartNum"].Value != null)
                {
                    if (dgr.Cells["PartNum"].Value.ToString() == partNum)
                    {
                        dgr.Selected = true;
                        break;
                    }
                }
            }
        }

        private void btnPRH_AddPart_Click(object sender, EventArgs e)
        {
            string partNum = "";
            
            frmUpdatePartRulesHead frmAdd = new frmUpdatePartRulesHead(this);

            DataTable dt = objPart.GetPartNumberList();

            frmAdd.cbPRH_PartNum.DataSource = dt;
            frmAdd.cbPRH_PartNum.DisplayMember = "PartNumDesc";
            frmAdd.cbPRH_PartNum.ValueMember = "PartNum";
            frmAdd.cbPRH_PartNum.SelectedIndex = 0;
           
            dt = objCat.MSP_GetPartCategoryList();
            frmAdd.cbPRH_PartCategory.DataSource = dt;
            frmAdd.cbPRH_PartCategory.DisplayMember = "CategoryName";
            frmAdd.cbPRH_PartCategory.ValueMember = "CategoryName";

            frmAdd.cbPRH_RelatedOp.SelectedIndex = 0;

            frmAdd.txtPRH_AsmSeq.Text = "0";

            frmAdd.btnSave.Text = "Add";
            frmAdd.btnDelete.Enabled = false;

            frmAdd.ShowDialog();

            partNum = mPartNumber;
            objPart.PartNum = null;
            dgvPartRulesHead.DataSource = null;
            dt = objPart.MSP_GetPartRulesHead();
            populatePartRuleHeadList(dt);

            foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
            {
                if (dgr.Cells["PartNum"].Value != null)
                {
                    if (dgr.Cells["PartNum"].Value.ToString() == partNum)
                    {
                        dgr.Selected = true;                       
                        break;
                    }
                }
            }
        }

        private void txtPRH_SearchPartNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {
                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.                

                tmpStr = this.txtPRH_SearchPartNum.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();

                foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
                {
                    if (dgr.Cells["PartNum"].Value != null)
                    {
                        if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                        {
                            dgr.Selected = true;
                            dgvPartRulesHead.FirstDisplayedScrollingRowIndex = dgr.Index;
                            break;
                        }
                    }
                }
            }
        }
        #endregion

        #region Part Conditions
        private void populatePartConditionsList(DataTable dt)
        {
            dgvPartsMain.DataSource = dt;

            dgvPartsMain.Columns["PartNum"].Width = 150;           // Part Number
            dgvPartsMain.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartsMain.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["PartNum"].DisplayIndex = 0;
            dgvPartsMain.Columns["RuleBatchID"].Width = 60;            // RuleBatchID
            dgvPartsMain.Columns["RuleBatchID"].HeaderText = "Rule\nBatch ID";
            dgvPartsMain.Columns["RuleBatchID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RuleBatchID"].DisplayIndex = 1;           
            dgvPartsMain.Columns["PartDescription"].Width = 300;           // Part Description
            dgvPartsMain.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartsMain.Columns["PartDescription"].DisplayIndex = 2;
            dgvPartsMain.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartsMain.Columns["RelatedOperation"].HeaderText = "Related\nOperation";
            dgvPartsMain.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RelatedOperation"].DisplayIndex = 3;
            dgvPartsMain.Columns["PartCategory"].Width = 150;           // PartCategory   
            dgvPartsMain.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartsMain.Columns["PartCategory"].DisplayIndex = 4;
            dgvPartsMain.Columns["Unit"].Width = 50;            // Unit Type          
            dgvPartsMain.Columns["Unit"].HeaderText = "Unit\nType";
            dgvPartsMain.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Unit"].DisplayIndex = 5;
            dgvPartsMain.Columns["Unit"].Visible = false;
            dgvPartsMain.Columns["Digit"].Width = 60;            // Digit
            dgvPartsMain.Columns["Digit"].HeaderText = "Digit";
            dgvPartsMain.Columns["Digit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Digit"].DisplayIndex = 6;
            dgvPartsMain.Columns["HeatType"].Width = 75;            // HeatType
            dgvPartsMain.Columns["HeatType"].HeaderText = "HeatType";
            dgvPartsMain.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["HeatType"].DisplayIndex = 7;
            dgvPartsMain.Columns["Value"].Width = 200;            // Value   
            dgvPartsMain.Columns["Value"].HeaderText = "Value";
            dgvPartsMain.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["Value"].DisplayIndex = 8;
            dgvPartsMain.Columns["Qty"].Width = 90;            // Qty
            dgvPartsMain.Columns["Qty"].HeaderText = "Quantity";
            dgvPartsMain.Columns["Qty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Qty"].DisplayIndex = 9;
            dgvPartsMain.Columns["DateMod"].Visible = false;
            dgvPartsMain.Columns["UserName"].Visible = false;
        }

        private void txtSearchPartNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.                

                tmpStr = this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();

                foreach (DataGridViewRow dgr in dgvPartsMain.Rows)
                {
                    if (dgr.Cells["PartNum"].Value != null)
                    {
                        if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                        {
                            dgr.Selected = true;
                            dgvPartsMain.FirstDisplayedScrollingRowIndex = dgr.Index;
                            break;
                        }
                    }
                }
            }
        }

        private void dgvPartsMain_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strValue = "";
            string partNumStr = "";
            string curPartNumStr = "";

            int iRuleId = 0;
            int iBatchId = 0;
            int iCurRuleId = -1;
            int iCurBatchId = -1;

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;
            Color nextBackGround = Color.White;

            foreach (DataGridViewRow row in dgvPartsMain.Rows)
            {
                partNumStr = row.Cells["PartNum"].Value.ToString();
               
                strValue = row.Cells["RuleBatchID"].Value.ToString();
                iBatchId = Int32.Parse(strValue);

                if ((curPartNumStr != partNumStr) || (iCurBatchId != iBatchId))
                {
                    curPartNumStr = partNumStr;
                    iCurBatchId = iBatchId;

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            //Load PartConditionsDTL DataGridView
            DataTable dt = objPart.MSP_GetPartsAndRulesList();
            populatePartConditionsList(dt);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtSearchPartNum.Text = "";
        }

        private void btnAddPart_Click(object sender, EventArgs e)
        {                     
            frmUpdatePartConditions frmNew = new frmUpdatePartConditions(this);

            objPart.PartNum = "";
            DataTable dt = objPart.MSP_GetPartRulesHead();
            frmNew.cbPartNum.DisplayMember = "PartNum";
            frmNew.cbPartNum.ValueMember = "PartNum";

            DataRow dr = dt.NewRow();
            //dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);

            objModel.ListType = "DistinctList";
            DataTable dt1 = objModel.MSP_GetModelNoList();

            frmNew.cbDigit.DisplayMember = "DigitHeat";
            frmNew.cbDigit.ValueMember = "HeatType";

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "Select Digit Number Please.";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);
            frmNew.cbDigit.DataSource = dt1;
           
            frmNew.cbDigit.SelectedIndex = 0;

            frmNew.gbValues.Visible = false;
            //frmNew.lbAddMsg.Text = "";
            frmNew.lbScreenMode.Text = "NewPartMode";
            frmNew.cbPartNum.Enabled = true;
            frmNew.cbPartNum.DataSource = dt;
            frmNew.cbPartNum.SelectedIndex = 0;
            frmNew.cbPartNum.Select();
            //frmNew.lbScreenMode.Text = "AddNewPart";
            frmNew.btnAddNewRule.Text = "Save New Rule";
            frmNew.btnUpdate.Enabled = false;
            frmNew.Text = "Add New Part";

            frmNew.ShowDialog();

            if (mUpdatedValueStr != "NoUpdate")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
        }

        private void dgvPartsMain_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum;
            string partDesc;
            string partCategory;
            string unitType;
            string value;
            string heatType;
            string modBy;
            string modDate;
           
            int ruleBatchID;
            int relatedOp;
            int digit;
            int selRowIdx = 0;
            int index = 0;

            decimal reqQty;
                        
            ruleBatchID = (int)dgvPartsMain.Rows[e.RowIndex].Cells["RuleBatchID"].Value;
            partNum = dgvPartsMain.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            partDesc = dgvPartsMain.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            relatedOp = (int)dgvPartsMain.Rows[e.RowIndex].Cells["RelatedOperation"].Value;
            partCategory = dgvPartsMain.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            unitType = dgvPartsMain.Rows[e.RowIndex].Cells["Unit"].Value.ToString();
            digit = (int)dgvPartsMain.Rows[e.RowIndex].Cells["Digit"].Value;
            heatType = dgvPartsMain.Rows[e.RowIndex].Cells["HeatType"].Value.ToString();
            value = dgvPartsMain.Rows[e.RowIndex].Cells["Value"].Value.ToString();
            reqQty = (decimal)dgvPartsMain.Rows[e.RowIndex].Cells["Qty"].Value;
            modDate = dgvPartsMain.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            modBy = dgvPartsMain.Rows[e.RowIndex].Cells["UserName"].Value.ToString();

            frmUpdatePartConditions frmUpd = new frmUpdatePartConditions(this);
            frmUpd.cbPartNum.Text = partNum;
            frmUpd.txtPartDesc.Text = partDesc;
            frmUpd.txtRelatedOp.Text = relatedOp.ToString();
            frmUpd.lbModBy.Text = modBy;
            frmUpd.lbModDate.Text = modDate;

            objModel.ListType = "DistinctList";
            DataTable dt1 = objModel.MSP_GetModelNoList();
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "DigitInt";

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "Select a DigitNo please.";
            dr1["DigitInt"] = 0;
            dt1.Rows.InsertAt(dr1, 0);

            frmUpd.cbDigit.DataSource = dt1;
            frmUpd.cbDigit.SelectedValue = digit;
            //frmUpd.cbDigit.Text = digit.ToString();
            frmUpd.txtPartCategory.Text = partCategory;

            //frmUpd.cbUnitType.Text = unitType;            
            frmUpd.txtBatchID.Text = ruleBatchID.ToString();
            frmUpd.nudReqQty.Value = reqQty;

            setupValueCheckBoxes(frmUpd, digit, value, heatType);

            objPart.PartNum = partNum;
            objPart.RuleBatchID = ruleBatchID.ToString();
            DataTable dt = objPart.MSP_GetPartRulesByPartNum_AndRuleBatchID();

            foreach (DataRow dr in dt.Rows)
            {
                index = frmUpd.dgvUpdateRules.Rows.Add();               
                
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = dr["RuleBatchID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Update";

                //if (value == dr["Value"].ToString())
                //{
                //    frmUpd.lbSelectedRow.Text = index.ToString();
                //}
                if (digit == Int32.Parse(dr["Digit"].ToString()))
                {
                    selRowIdx = index;
                    frmUpd.lbSelectedRow.Text = index.ToString();
                }
            }

            frmUpd.dgvUpdateRules.FirstDisplayedScrollingRowIndex = selRowIdx;

            frmUpd.lbScreenMode.Text = "UpdateMode";
            this.btnMainExit.Enabled = false;
            frmUpd.ShowDialog();

            if (mUpdatedValueStr != "NoUpdate")
            {
                //dgvPartsMain.Rows[e.RowIndex].Cells[6].Value = Int32.Parse(mRelatedOp);
                //dgvPartsMain.Rows[e.RowIndex].Cells[10].Value = mUpdatedValueStr;
                //dgvPartsMain.Rows[e.RowIndex].Cells[11].Value = Decimal.Parse(mQty);

                dt = objPart.MSP_GetPartsAndRulesList();
                populatePartConditionsList(dt);

                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
            this.btnMainExit.Enabled = true;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {                   
            int rowIdx = dgvPartsMain.CurrentRow.Index;   // Currently selected row

            int iDigit = 0;

            bool firstRow = true;

            frmUpdatePartConditions frmUpd = new frmUpdatePartConditions(this);

            mRuleBatchID = dgvPartsMain.Rows[rowIdx].Cells["RuleBatchID"].Value.ToString();
            mPartNumber = dgvPartsMain.Rows[rowIdx].Cells["PartNum"].Value.ToString();

            objPart.PartNum = mPartNumber;
            objPart.MSP_GetPartNumberInfo();

            objPart.PartCategory = null;
            DataTable dt = objPart.MSP_GetMainPartList();
            frmUpd.cbPartNum.DisplayMember = "PartNum";
            frmUpd.cbPartNum.ValueMember = "ID";

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);
            frmUpd.cbPartNum.DataSource = dt;
            frmUpd.cbPartNum.SelectedIndex = 0;
            frmUpd.cbPartNum.Text = mPartNumber;
            frmUpd.cbPartNum.Enabled = true;
            //frmUpd.cbUnitType.Text = mUnitType;
            frmUpd.txtRelatedOp.Text = mRelatedOp;
            frmUpd.txtPartDesc.Text = dgvPartsMain.Rows[rowIdx].Cells["PartDescription"].Value.ToString();

            frmUpd.txtBatchID.Text = objPart.RuleBatchID.ToString();

            objModel.ListType = "DistinctList";
            DataTable dt1 = objModel.MSP_GetModelNoList();
            frmUpd.cbDigit.DataSource = dt1;
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "HeatType";
            frmUpd.cbDigit.SelectedIndex = 0;

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "0/NA = CircuitBreaker";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            objPart.PartNum = mPartNumber;
            objPart.RuleBatchID = mRuleBatchID;
            dt = objPart.MSP_GetPartRulesByPartNum_AndRuleBatchID();

            foreach (DataRow dr2 in dt.Rows)
            {
                if (firstRow)
                {
                    mDigit = dr2["Digit"].ToString();
                    mHeatType = dr2["HeatType"].ToString();
                    mValueStr = dr2["Value"].ToString();
                    mQty = dr2["Qty"].ToString();
                    firstRow = false;
                }

                int index = frmUpd.dgvUpdateRules.Rows.Add();

                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = frmUpd.txtBatchID.Text;
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr2["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr2["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr2["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr2["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr2["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr2["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr2["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr2["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr2["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Insert";
            }

            int selIdx = objPart.getDigitSelectedIndex(mDigit, mHeatType, frmUpd.cbDigit);

            frmUpd.cbDigit.SelectedIndex = selIdx;
            iDigit = Int32.Parse(mDigit);

            frmUpd.gbValues.Enabled = true;
            frmUpd.gbValues.Visible = true;

            //objPart.GetOAU_PartNumberInfo();

            frmUpd.txtPartDesc.Text = mPartDesc;
            frmUpd.txtPartCategory.Text = mPartCategory;

            frmUpd.txtBatchID.Text = "00";
            frmUpd.nudReqQty.Value = decimal.Parse(mQty);

            setupValueCheckBoxesCopyRule(frmUpd, iDigit, mValueStr, mHeatType);

            this.btnMainExit.Enabled = false;
            frmUpd.lbScreenMode.Text = "CopyMode";
            frmUpd.btnUpdate.Enabled = false;
            frmUpd.btnAllRuleSets.Enabled = true;
            frmUpd.btnSave.Enabled = false;

            frmUpd.cbDigit.Enabled = false;

            frmUpd.Text = "Copy Rule Set";
            frmUpd.ShowDialog();

            btnMainExit.Enabled = true;

            if (mQty != "-1")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
        }
      
        #endregion

        #region ModelNo
        private void populateModelNoList(DataTable dt)
        {
            dgvModelNo.DataSource = dt;

            dgvModelNo.Columns["DigitNo"].Width = 50;            // DigitNo
            dgvModelNo.Columns["DigitNo"].HeaderText = "Digit No";
            dgvModelNo.Columns["DigitNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvModelNo.Columns["DigitNo"].DisplayIndex = 0;
            dgvModelNo.Columns["DigitVal"].Width = 50;            // Digit Value
            dgvModelNo.Columns["DigitVal"].HeaderText = "Digit\nValue";
            dgvModelNo.Columns["DigitVal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitVal"].DisplayIndex = 1;
            dgvModelNo.Columns["DigitDescription"].Width = 250;           // Digit Description
            dgvModelNo.Columns["DigitDescription"].HeaderText = "Digit Description";
            dgvModelNo.Columns["DigitDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitDescription"].DisplayIndex = 2;
            dgvModelNo.Columns["HeatType"].Width = 75;           //Heat Type
            dgvModelNo.Columns["HeatType"].HeaderText = "Heat Type";
            dgvModelNo.Columns["HeatType"].DisplayIndex = 3;
            dgvModelNo.Columns["DigitRepresentation"].Width = 200;            //DigitRepresentation
            dgvModelNo.Columns["DigitRepresentation"].HeaderText = "Digit Representation";
            dgvModelNo.Columns["DigitRepresentation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitRepresentation"].DisplayIndex = 4;
            dgvModelNo.Columns["ID"].Visible = false;            
            dgvModelNo.Columns["DigitInt"].Visible = false;
            dgvModelNo.Columns["DigitHeat"].Visible = false;
        }

        private void txtSearchModelNo_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";           

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.                

                tmpStr = this.txtSearchModelNo.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();

                foreach (DataGridViewRow dgr in dgvModelNo.Rows)
                {
                    if (dgr.Cells["DigitNo"].Value != null)
                    {
                        if (dgr.Cells["DigitNo"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["DigitNo"].Value.ToString() == tmpStr)
                        {
                            dgr.Selected = true;
                            dgvModelNo.FirstDisplayedScrollingRowIndex = dgr.Index;
                            break;
                        }
                    }
                }

            }
        }

        private void btnModelNoReset_Click(object sender, EventArgs e)
        {
            txtSearchModelNo.Text = "";
            objModel.ListType = "FullList";
            DataTable dt = objModel.MSP_GetModelNoList();
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt);
        }    
        private void dgvModelNo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string digitValue;
            string heatType;
            string digitDesc;
            string digitRep;
            string idStr;

            int digitNo;
            int ID;
            int rowIdx = e.RowIndex;

            digitNo = (int)dgvModelNo.Rows[rowIdx].Cells["DigitNo"].Value;
            digitValue = dgvModelNo.Rows[rowIdx].Cells["DigitVal"].Value.ToString();
            digitDesc = dgvModelNo.Rows[rowIdx].Cells["DigitDescription"].Value.ToString();
            heatType = dgvModelNo.Rows[rowIdx].Cells["HeatType"].Value.ToString();
            digitRep = dgvModelNo.Rows[rowIdx].Cells["DigitRepresentation"].Value.ToString();
            idStr = dgvModelNo.Rows[rowIdx].Cells["ID"].Value.ToString();
            ID = Int32.Parse(idStr);

            frmUpdateModelNo frmUpd = new frmUpdateModelNo(this);
            frmUpd.txtModelNoUpdDigitNo.Text = digitNo.ToString();
            frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
            frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
            frmUpd.txtModelNoUpdDigitRep.Text = digitRep;
            frmUpd.lbModelNoID.Text = ID.ToString();
            frmUpd.gbModelNoHeatType.Visible = true;
            frmUpd.btnSave.Text = "Update";

            frmUpd.rbModelNoHeatTypeNA.Checked = true;
            if (heatType == "NA")
            {
                frmUpd.rbModelNoHeatTypeNA.Checked = true;
            }
            else if (heatType == "GE")
            {
                frmUpd.rbModelNoHeatTypeGE.Checked = true;
            }
            else if (heatType == "ELEC")
            {
                frmUpd.rbModelNoHeatTypeElec.Checked = true;
            }

            frmUpd.ShowDialog();

            if (mHeatType == "Delete")
            {
                dgvModelNo.Rows.RemoveAt(rowIdx);
                dgvModelNo.Refresh();
            }
            else if (mHeatType != "NoUpdate")
            {
                dgvModelNo.Rows[rowIdx].Cells["DigitVal"].Value = mDigitVal;
                dgvModelNo.Rows[rowIdx].Cells["DigitDescription"].Value = mDigitDesc;
                dgvModelNo.Rows[rowIdx].Cells["HeatType"].Value = mHeatType;
                dgvModelNo.Rows[rowIdx].Cells["DigitRepresentation"].Value = mDigitRep;
                dgvModelNo.Refresh();
            }
        }
        private void btnModelNoAddRow_Click(object sender, EventArgs e)
        {
            frmUpdateModelNo frmUpd = new frmUpdateModelNo(this);

            frmUpd.txtModelNoUpdDigitNo.Enabled = true;
            frmUpd.btnDelete.Enabled = false;
            frmUpd.btnSave.Text = "Add";
            frmUpd.Text = "Add ModelNo";
            frmUpd.txtModelNoUpdDigitNo.Focus();
            frmUpd.txtModelNoUpdDigitNo.Select();
            frmUpd.gbModelNoHeatType.Visible = true;
            frmUpd.ShowDialog();


            if (mHeatType != "NoUpdate")
            {
                //txtSearchModelNo.Text = "";
                dgvModelNo.DataSource = null;
                objModel.ListType = "FullList";
                DataTable dt = objModel.MSP_GetModelNoList();
                //dgvModelNo.DataSource = dt;
                populateModelNoList(dt);
                foreach (DataGridViewRow row in dgvModelNo.Rows)
                {
                    if (row.Cells["DigitNo"].Value.ToString() == mDigitNo)
                    {
                        row.Selected = true;
                        dgvModelNo.FirstDisplayedScrollingRowIndex = row.Index;
                        break;
                    }
                }
            }            
        }

        private void dgvModelNo_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strDigitVal = "";

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;
            Color nextBackGround = Color.White;

            foreach (DataGridViewRow row in dgvModelNo.Rows)
            {
                if (row.Cells["DigitNo"].Value.ToString() != strDigitVal)
                {
                    strDigitVal = row.Cells["DigitNo"].Value.ToString();

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }

        #endregion

        #region PartCategory
        private void btnPartCatAdd_Click(object sender, EventArgs e)
        {
            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.btnUpdPartCatSave.Text = "Add";
            frmUpd.Text = "Add Part Category Head";
            frmUpd.btnPartCatDelete.Enabled = false;
            frmUpd.ShowDialog();

            if (mVolts != "NoUpdate")
            {
                objCat.PullOrder = "-1";
                DataTable dt = objCat.MSP_GetPartCategoryHead();
                populatePartCategoryDataList(dt);
            }
        }

        private void btnPartCatReset_Click(object sender, EventArgs e)
        {
            objCat.PullOrder = "-1";
            DataTable dt = objCat.MSP_GetPartCategoryHead();
            populatePartCategoryDataList(dt);
        }

        private void dgvPartCat_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sCategoryName;
            string sCategoryDesc;
            string sModBy;
            string sLastModDate;
            string sCritComp;

            int iPullOrder;
            int iID;
            int rowIdx = e.RowIndex;

            sCategoryName = dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();
            sCategoryDesc = dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value.ToString();
            iPullOrder = (int)dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value;
            sCritComp = dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value.ToString();
            sModBy = dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvPartCat.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.lbUpdPartCatID.Text = iID.ToString();
            frmUpd.txtUpdPartCat_CategoryName.Text = sCategoryName;
            frmUpd.txtUpdPartCat_CategoryDesc.Text = sCategoryDesc;
            frmUpd.txtUpdPartCatPullOrder.Text = iPullOrder.ToString();

            if (sCritComp == "True")
            {
                frmUpd.cbUpdPartCatCritComp.Checked = true;
            }
            else
            {
                frmUpd.cbUpdPartCatCritComp.Checked = false;
            }

            frmUpd.txtUpdPartCatModBy.Text = sModBy;
            frmUpd.txtUpdPartCatLastModDate.Text = sLastModDate;
            frmUpd.btnUpdPartCatSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mPullOrder == "Delete")
            {
                dgvPartCat.Rows.RemoveAt(rowIdx);
                dgvPartCat.Refresh();
            }
            else if (mPullOrder != "NoUpdate")
            {
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value = mCategoryName;
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value = mCategoryDesc;
                dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value = mPullOrder;
                dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value = mCritComp;
                dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvPartCat.Refresh();
            }
        }

        private void populatePartCategoryDataList(DataTable dt)
        {
            dgvPartCat.DataSource = dt;

            dgvPartCat.Columns["CategoryName"].Width = 150;            // CategoryName
            dgvPartCat.Columns["CategoryName"].HeaderText = "Category Name";
            dgvPartCat.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryName"].DisplayIndex = 0;
            dgvPartCat.Columns["CategoryDesc"].Width = 200;            // CategoryDesc
            dgvPartCat.Columns["CategoryDesc"].HeaderText = "Category Desc";
            dgvPartCat.Columns["CategoryDesc"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryDesc"].DisplayIndex = 1;
            dgvPartCat.Columns["PullOrder"].Width = 45;            // Pull Order
            dgvPartCat.Columns["PullOrder"].HeaderText = "Pull\nOrder";
            dgvPartCat.Columns["PullOrder"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartCat.Columns["PullOrder"].DisplayIndex = 2;
            dgvPartCat.Columns["CriticalComp"].Width = 70;            //CriticalComponent
            dgvPartCat.Columns["CriticalComp"].HeaderText = "Critical\nComponent";
            dgvPartCat.Columns["CriticalComp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartCat.Columns["CriticalComp"].DisplayIndex = 3;
            dgvPartCat.Columns["UserName"].Width = 100;            //UserName
            dgvPartCat.Columns["UserName"].HeaderText = "UserName";
            dgvPartCat.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["UserName"].DisplayIndex = 4;
            dgvPartCat.Columns["DateMod"].Width = 125;           // Date Modified  
            dgvPartCat.Columns["DateMod"].HeaderText = "Date Modified";
            dgvPartCat.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["DateMod"].DisplayIndex = 5;
            dgvPartCat.Columns["ID"].Visible = false;

        }
        #endregion

        #region Other Methods
        private string convertKeyValue(int keyValue)
        {
            string retVal = "";
            // This function converts the integer KeyValue passed into it to its string equivalent.

            switch (keyValue)
            {
                case 48:
                case 96:
                    retVal = "0";
                    break;
                case 49:
                case 97:
                    retVal = "1";
                    break;
                case 50:
                case 98:
                    retVal = "2";
                    break;
                case 51:
                case 99:
                    retVal = "3";
                    break;
                case 52:
                case 100:
                    retVal = "4";
                    break;
                case 53:
                case 101:
                    retVal = "5";
                    break;
                case 54:
                case 102:
                    retVal = "6";
                    break;
                case 55:
                case 103:
                    retVal = "7";
                    break;
                case 56:
                case 104:
                    retVal = "8";
                    break;
                case 57:
                case 105:
                    retVal = "9";
                    break;
                case 65:
                    retVal = "a";
                    break;
                case 66:
                    retVal = "b";
                    break;
                case 67:
                    retVal = "c";
                    break;
                case 68:
                    retVal = "d";
                    break;
                case 69:
                    retVal = "e";
                    break;
                case 70:
                    retVal = "f";
                    break;
                case 71:
                    retVal = "g";
                    break;
                case 72:
                    retVal = "h";
                    break;
                case 73:
                    retVal = "i";
                    break;
                case 74:
                    retVal = "j";
                    break;
                case 75:
                    retVal = "k";
                    break;
                case 76:
                    retVal = "l";
                    break;
                case 77:
                    retVal = "m";
                    break;
                case 78:
                    retVal = "n";
                    break;
                case 79:
                    retVal = "o";
                    break;
                case 80:
                    retVal = "p";
                    break;
                case 81:
                    retVal = "q";
                    break;
                case 82:
                    retVal = "r";
                    break;
                case 83:
                    retVal = "s";
                    break;
                case 84:
                    retVal = "t";
                    break;
                case 85:
                    retVal = "u";
                    break;
                case 86:
                    retVal = "v";
                    break;
                case 87:
                    retVal = "w";
                    break;
                case 88:
                    retVal = "x";
                    break;
                case 89:
                    retVal = "y";
                    break;
                case 90:
                    retVal = "z";
                    break;
                default:
                    retVal = "";
                    break;
            }

            return retVal;
        }

        private void positionDataGrid(string partNum, int? ruleBatchID)
        {            
            dgvPartsMain.DataSource = null;
            DataTable dtPCL = objPart.MSP_GetPartsAndRulesList();
            dgvPartsMain.DataSource = dtPCL;
            populatePartConditionsList(dtPCL);

            if (ruleBatchID != null)
            {
                foreach (DataGridViewRow row in dgvPartsMain.Rows)
                {
                    if (row.Cells["PartNum"].Value != null)
                    {
                        if (row.Cells["PartNum"].Value.ToString() == partNum && Int32.Parse(row.Cells["RuleBatchID"].Value.ToString()) == ruleBatchID)
                        {                           
                            row.Selected = true;
                            dgvPartsMain.FirstDisplayedScrollingRowIndex = row.Index;
                            break;
                        }
                    }
                }
            }
            else
            {
                dgvPartsMain.Rows[0].Selected = true;
            }
        }

        private void setupValueCheckBoxes(frmUpdatePartConditions frmUpd, int digit, string actValueStr, string heatType)
        {
            int idx = 0;
            string valueStr = "";

            objModel.DigitNo = digit.ToString();
            objModel.HeatType = heatType;
            DataTable dt = objModel.MSP_GetModelNumDesc();

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmUpd.lbVal1);
            labelControls.Add(frmUpd.lbVal2);
            labelControls.Add(frmUpd.lbVal3);
            labelControls.Add(frmUpd.lbVal4);
            labelControls.Add(frmUpd.lbVal5);
            labelControls.Add(frmUpd.lbVal6);
            labelControls.Add(frmUpd.lbVal7);
            labelControls.Add(frmUpd.lbVal8);
            labelControls.Add(frmUpd.lbVal9);
            labelControls.Add(frmUpd.lbVal10);
            labelControls.Add(frmUpd.lbVal11);
            labelControls.Add(frmUpd.lbVal12);
            labelControls.Add(frmUpd.lbVal13);
            labelControls.Add(frmUpd.lbVal14);
            labelControls.Add(frmUpd.lbVal15);
            labelControls.Add(frmUpd.lbVal16);
            labelControls.Add(frmUpd.lbVal17);
            labelControls.Add(frmUpd.lbVal18);
            labelControls.Add(frmUpd.lbVal19);
            labelControls.Add(frmUpd.lbVal20);
            labelControls.Add(frmUpd.lbVal21);
            labelControls.Add(frmUpd.lbVal22);
            labelControls.Add(frmUpd.lbVal23);
            labelControls.Add(frmUpd.lbVal24);
            labelControls.Add(frmUpd.lbVal25);
            labelControls.Add(frmUpd.lbVal26);
            labelControls.Add(frmUpd.lbVal27);
            labelControls.Add(frmUpd.lbVal28);
            labelControls.Add(frmUpd.lbVal29);
            labelControls.Add(frmUpd.lbVal30);

            checkBoxControls.Add(frmUpd.cbVal1);
            checkBoxControls.Add(frmUpd.cbVal2);
            checkBoxControls.Add(frmUpd.cbVal3);
            checkBoxControls.Add(frmUpd.cbVal4);
            checkBoxControls.Add(frmUpd.cbVal5);
            checkBoxControls.Add(frmUpd.cbVal6);
            checkBoxControls.Add(frmUpd.cbVal7);
            checkBoxControls.Add(frmUpd.cbVal8);
            checkBoxControls.Add(frmUpd.cbVal9);
            checkBoxControls.Add(frmUpd.cbVal10);
            checkBoxControls.Add(frmUpd.cbVal11);
            checkBoxControls.Add(frmUpd.cbVal12);
            checkBoxControls.Add(frmUpd.cbVal13);
            checkBoxControls.Add(frmUpd.cbVal14);
            checkBoxControls.Add(frmUpd.cbVal15);
            checkBoxControls.Add(frmUpd.cbVal16);
            checkBoxControls.Add(frmUpd.cbVal17);
            checkBoxControls.Add(frmUpd.cbVal18);
            checkBoxControls.Add(frmUpd.cbVal19);
            checkBoxControls.Add(frmUpd.cbVal20);
            checkBoxControls.Add(frmUpd.cbVal21);
            checkBoxControls.Add(frmUpd.cbVal22);
            checkBoxControls.Add(frmUpd.cbVal23);
            checkBoxControls.Add(frmUpd.cbVal24);
            checkBoxControls.Add(frmUpd.cbVal25);
            checkBoxControls.Add(frmUpd.cbVal26);
            checkBoxControls.Add(frmUpd.cbVal27);
            checkBoxControls.Add(frmUpd.cbVal28);
            checkBoxControls.Add(frmUpd.cbVal29);
            checkBoxControls.Add(frmUpd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Visible = true;
                frmUpd.toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.SetToolTip(checkBoxControls[idx++], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.ShowAlways = true;
            }
        }

        private void setupValueCheckBoxesCopyRule(frmUpdatePartConditions frmAdd, int digit, string actValueStr, string heatType)
        {
            int idx = 0;
            string valueStr = "";

            objModel.DigitNo = digit.ToString();
            objModel.HeatType = heatType;
            DataTable dt = objModel.MSP_GetModelNumDesc();

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmAdd.lbVal1);
            labelControls.Add(frmAdd.lbVal2);
            labelControls.Add(frmAdd.lbVal3);
            labelControls.Add(frmAdd.lbVal4);
            labelControls.Add(frmAdd.lbVal5);
            labelControls.Add(frmAdd.lbVal6);
            labelControls.Add(frmAdd.lbVal7);
            labelControls.Add(frmAdd.lbVal8);
            labelControls.Add(frmAdd.lbVal9);
            labelControls.Add(frmAdd.lbVal10);
            labelControls.Add(frmAdd.lbVal11);
            labelControls.Add(frmAdd.lbVal12);
            labelControls.Add(frmAdd.lbVal13);
            labelControls.Add(frmAdd.lbVal14);
            labelControls.Add(frmAdd.lbVal15);
            labelControls.Add(frmAdd.lbVal16);
            labelControls.Add(frmAdd.lbVal17);
            labelControls.Add(frmAdd.lbVal18);
            labelControls.Add(frmAdd.lbVal19);
            labelControls.Add(frmAdd.lbVal20);
            labelControls.Add(frmAdd.lbVal21);
            labelControls.Add(frmAdd.lbVal22);
            labelControls.Add(frmAdd.lbVal23);
            labelControls.Add(frmAdd.lbVal24);
            labelControls.Add(frmAdd.lbVal25);
            labelControls.Add(frmAdd.lbVal26);
            labelControls.Add(frmAdd.lbVal27);
            labelControls.Add(frmAdd.lbVal28);
            labelControls.Add(frmAdd.lbVal29);
            labelControls.Add(frmAdd.lbVal30);

            checkBoxControls.Add(frmAdd.cbVal1);
            checkBoxControls.Add(frmAdd.cbVal2);
            checkBoxControls.Add(frmAdd.cbVal3);
            checkBoxControls.Add(frmAdd.cbVal4);
            checkBoxControls.Add(frmAdd.cbVal5);
            checkBoxControls.Add(frmAdd.cbVal6);
            checkBoxControls.Add(frmAdd.cbVal7);
            checkBoxControls.Add(frmAdd.cbVal8);
            checkBoxControls.Add(frmAdd.cbVal9);
            checkBoxControls.Add(frmAdd.cbVal10);
            checkBoxControls.Add(frmAdd.cbVal11);
            checkBoxControls.Add(frmAdd.cbVal12);
            checkBoxControls.Add(frmAdd.cbVal13);
            checkBoxControls.Add(frmAdd.cbVal14);
            checkBoxControls.Add(frmAdd.cbVal15);
            checkBoxControls.Add(frmAdd.cbVal16);
            checkBoxControls.Add(frmAdd.cbVal17);
            checkBoxControls.Add(frmAdd.cbVal18);
            checkBoxControls.Add(frmAdd.cbVal19);
            checkBoxControls.Add(frmAdd.cbVal20);
            checkBoxControls.Add(frmAdd.cbVal21);
            checkBoxControls.Add(frmAdd.cbVal22);
            checkBoxControls.Add(frmAdd.cbVal23);
            checkBoxControls.Add(frmAdd.cbVal24);
            checkBoxControls.Add(frmAdd.cbVal25);
            checkBoxControls.Add(frmAdd.cbVal26);
            checkBoxControls.Add(frmAdd.cbVal27);
            checkBoxControls.Add(frmAdd.cbVal28);
            checkBoxControls.Add(frmAdd.cbVal29);
            checkBoxControls.Add(frmAdd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Enabled = true;
                checkBoxControls[idx++].Visible = true;
            }

        }
        #endregion

        
    }
}
