﻿namespace MSP_PartMaintenanceTool
{
    partial class frmUpdatePartRulesHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPRH_AsmSeq = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtPRH_PartDesc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPRH_DateModified = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPRH_ModBy = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.cbPRH_RelatedOp = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbPRH_PartCategory = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbID = new System.Windows.Forms.Label();
            this.cbPRH_PartNum = new System.Windows.Forms.ComboBox();
            this.txtPRH_LinesideBin = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPRH_StationLocation = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkBoxPicklist = new System.Windows.Forms.CheckBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtPartNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtPRH_AsmSeq
            // 
            this.txtPRH_AsmSeq.Location = new System.Drawing.Point(155, 129);
            this.txtPRH_AsmSeq.Name = "txtPRH_AsmSeq";
            this.txtPRH_AsmSeq.Size = new System.Drawing.Size(57, 20);
            this.txtPRH_AsmSeq.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(8, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 144;
            this.label7.Text = "Assembly Seq:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(364, 221);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtPRH_PartDesc
            // 
            this.txtPRH_PartDesc.Enabled = false;
            this.txtPRH_PartDesc.Location = new System.Drawing.Point(155, 49);
            this.txtPRH_PartDesc.Name = "txtPRH_PartDesc";
            this.txtPRH_PartDesc.Size = new System.Drawing.Size(243, 20);
            this.txtPRH_PartDesc.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(8, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 20);
            this.label6.TabIndex = 141;
            this.label6.Text = "Part Description:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_DateModified
            // 
            this.txtPRH_DateModified.Enabled = false;
            this.txtPRH_DateModified.Location = new System.Drawing.Point(155, 233);
            this.txtPRH_DateModified.Name = "txtPRH_DateModified";
            this.txtPRH_DateModified.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_DateModified.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(8, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 20);
            this.label2.TabIndex = 139;
            this.label2.Text = "Date Modified:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_ModBy
            // 
            this.txtPRH_ModBy.Enabled = false;
            this.txtPRH_ModBy.Location = new System.Drawing.Point(155, 207);
            this.txtPRH_ModBy.Name = "txtPRH_ModBy";
            this.txtPRH_ModBy.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_ModBy.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(8, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 137;
            this.label1.Text = "Last Modified By:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Green;
            this.btnSave.Location = new System.Drawing.Point(364, 140);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Add";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCancel.ForeColor = System.Drawing.Color.Red;
            this.btnUpdCancel.Location = new System.Drawing.Point(364, 181);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCancel.TabIndex = 12;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // cbPRH_RelatedOp
            // 
            this.cbPRH_RelatedOp.FormattingEnabled = true;
            this.cbPRH_RelatedOp.Items.AddRange(new object[] {
            "Select Related Operation",
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90",
            "100",
            "110"});
            this.cbPRH_RelatedOp.Location = new System.Drawing.Point(155, 102);
            this.cbPRH_RelatedOp.Name = "cbPRH_RelatedOp";
            this.cbPRH_RelatedOp.Size = new System.Drawing.Size(170, 21);
            this.cbPRH_RelatedOp.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(8, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 131;
            this.label8.Text = "Related Operation:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPRH_PartCategory
            // 
            this.cbPRH_PartCategory.FormattingEnabled = true;
            this.cbPRH_PartCategory.Location = new System.Drawing.Point(155, 75);
            this.cbPRH_PartCategory.Name = "cbPRH_PartCategory";
            this.cbPRH_PartCategory.Size = new System.Drawing.Size(170, 21);
            this.cbPRH_PartCategory.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(8, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 20);
            this.label4.TabIndex = 129;
            this.label4.Text = "Part Category:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(8, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 127;
            this.label3.Text = "Part Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbID
            // 
            this.lbID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbID.Location = new System.Drawing.Point(12, 9);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(30, 20);
            this.lbID.TabIndex = 135;
            this.lbID.Text = "ID:";
            this.lbID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbID.Visible = false;
            // 
            // cbPRH_PartNum
            // 
            this.cbPRH_PartNum.DropDownWidth = 600;
            this.cbPRH_PartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPRH_PartNum.FormattingEnabled = true;
            this.cbPRH_PartNum.Location = new System.Drawing.Point(155, 20);
            this.cbPRH_PartNum.Name = "cbPRH_PartNum";
            this.cbPRH_PartNum.Size = new System.Drawing.Size(243, 23);
            this.cbPRH_PartNum.TabIndex = 1;
            this.cbPRH_PartNum.SelectedIndexChanged += new System.EventHandler(this.cbPRH_PartNum_SelectedIndexChanged);
            // 
            // txtPRH_LinesideBin
            // 
            this.txtPRH_LinesideBin.Location = new System.Drawing.Point(155, 181);
            this.txtPRH_LinesideBin.Name = "txtPRH_LinesideBin";
            this.txtPRH_LinesideBin.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_LinesideBin.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(8, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 20);
            this.label5.TabIndex = 149;
            this.label5.Text = "Lineside Bin:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_StationLocation
            // 
            this.txtPRH_StationLocation.Location = new System.Drawing.Point(155, 155);
            this.txtPRH_StationLocation.Name = "txtPRH_StationLocation";
            this.txtPRH_StationLocation.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_StationLocation.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(8, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 20);
            this.label9.TabIndex = 147;
            this.label9.Text = "Station Location:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkBoxPicklist
            // 
            this.chkBoxPicklist.AutoSize = true;
            this.chkBoxPicklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxPicklist.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxPicklist.Location = new System.Drawing.Point(253, 130);
            this.chkBoxPicklist.Name = "chkBoxPicklist";
            this.chkBoxPicklist.Size = new System.Drawing.Size(72, 19);
            this.chkBoxPicklist.TabIndex = 6;
            this.chkBoxPicklist.Text = "Picklist";
            this.chkBoxPicklist.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Green;
            this.btnReset.Location = new System.Drawing.Point(364, 100);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 30);
            this.btnReset.TabIndex = 150;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtPartNum
            // 
            this.txtPartNum.Enabled = false;
            this.txtPartNum.Location = new System.Drawing.Point(155, 21);
            this.txtPartNum.Name = "txtPartNum";
            this.txtPartNum.Size = new System.Drawing.Size(243, 20);
            this.txtPartNum.TabIndex = 151;
            this.txtPartNum.Visible = false;
            // 
            // frmUpdatePartRulesHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 274);
            this.Controls.Add(this.txtPartNum);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.chkBoxPicklist);
            this.Controls.Add(this.txtPRH_LinesideBin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPRH_StationLocation);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbPRH_PartNum);
            this.Controls.Add(this.txtPRH_AsmSeq);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtPRH_PartDesc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPRH_DateModified);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPRH_ModBy);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbID);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Controls.Add(this.cbPRH_RelatedOp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbPRH_PartCategory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "frmUpdatePartRulesHead";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Part Rules Head";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtPRH_AsmSeq;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtPRH_PartDesc;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtPRH_DateModified;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtPRH_ModBy;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.ComboBox cbPRH_RelatedOp;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cbPRH_PartCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cbPRH_PartNum;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.TextBox txtPRH_LinesideBin;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtPRH_StationLocation;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.CheckBox chkBoxPicklist;
        public System.Windows.Forms.Label lbID;
        public System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.TextBox txtPartNum;
    }
}