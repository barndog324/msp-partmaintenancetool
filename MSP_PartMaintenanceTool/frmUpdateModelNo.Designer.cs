﻿namespace MSP_PartMaintenanceTool
{
    partial class frmUpdateModelNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.lbModelNoID = new System.Windows.Forms.Label();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeGE = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.txtModelNoUpdDigitRep = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitVal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.gbModelNoHeatType.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(604, 96);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 25);
            this.btnDelete.TabIndex = 132;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lbModelNoID
            // 
            this.lbModelNoID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNoID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbModelNoID.Location = new System.Drawing.Point(505, 96);
            this.lbModelNoID.Name = "lbModelNoID";
            this.lbModelNoID.Size = new System.Drawing.Size(31, 20);
            this.lbModelNoID.TabIndex = 131;
            this.lbModelNoID.Text = "ID:";
            this.lbModelNoID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbModelNoID.Visible = false;
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeGE);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(383, 21);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(201, 62);
            this.gbModelNoHeatType.TabIndex = 130;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            this.gbModelNoHeatType.Visible = false;
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 19);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeGE
            // 
            this.rbModelNoHeatTypeGE.AutoSize = true;
            this.rbModelNoHeatTypeGE.Location = new System.Drawing.Point(107, 19);
            this.rbModelNoHeatTypeGE.Name = "rbModelNoHeatTypeGE";
            this.rbModelNoHeatTypeGE.Size = new System.Drawing.Size(84, 17);
            this.rbModelNoHeatTypeGE.TabIndex = 8;
            this.rbModelNoHeatTypeGE.Text = "Gas/Electric";
            this.rbModelNoHeatTypeGE.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(7, 37);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            // 
            // txtModelNoUpdDigitRep
            // 
            this.txtModelNoUpdDigitRep.Location = new System.Drawing.Point(187, 99);
            this.txtModelNoUpdDigitRep.Name = "txtModelNoUpdDigitRep";
            this.txtModelNoUpdDigitRep.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitRep.TabIndex = 129;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(25, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 20);
            this.label3.TabIndex = 128;
            this.label3.Text = "Digit Representation:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitDesc
            // 
            this.txtModelNoUpdDigitDesc.Location = new System.Drawing.Point(187, 73);
            this.txtModelNoUpdDigitDesc.Name = "txtModelNoUpdDigitDesc";
            this.txtModelNoUpdDigitDesc.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitDesc.TabIndex = 127;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(25, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 126;
            this.label2.Text = "Digit Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitVal
            // 
            this.txtModelNoUpdDigitVal.Location = new System.Drawing.Point(187, 47);
            this.txtModelNoUpdDigitVal.Name = "txtModelNoUpdDigitVal";
            this.txtModelNoUpdDigitVal.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitVal.TabIndex = 125;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(25, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 124;
            this.label1.Text = "Digit Value:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitNo
            // 
            this.txtModelNoUpdDigitNo.Enabled = false;
            this.txtModelNoUpdDigitNo.Location = new System.Drawing.Point(187, 21);
            this.txtModelNoUpdDigitNo.Name = "txtModelNoUpdDigitNo";
            this.txtModelNoUpdDigitNo.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitNo.TabIndex = 123;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(25, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 122;
            this.label5.Text = "Digit No:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Green;
            this.btnSave.Location = new System.Drawing.Point(604, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 121;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCancel.ForeColor = System.Drawing.Color.Red;
            this.btnUpdCancel.Location = new System.Drawing.Point(604, 58);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 25);
            this.btnUpdCancel.TabIndex = 120;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // frmUpdateModelNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 142);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lbModelNoID);
            this.Controls.Add(this.gbModelNoHeatType);
            this.Controls.Add(this.txtModelNoUpdDigitRep);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtModelNoUpdDigitDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtModelNoUpdDigitVal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtModelNoUpdDigitNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Name = "frmUpdateModelNo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update ModelNo";
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Label lbModelNoID;
        public System.Windows.Forms.GroupBox gbModelNoHeatType;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeGE;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitRep;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitDesc;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitVal;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitNo;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnUpdCancel;
    }
}