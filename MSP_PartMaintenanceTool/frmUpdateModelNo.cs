﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSP_PartMaintenanceTool
{
    public partial class frmUpdateModelNo : Form
    {
        ModelNoBO objModel = new ModelNoBO();

        private frmMain m_parent;

        public frmUpdateModelNo(frmMain frmMn)
        {
            InitializeComponent();
            m_parent = frmMn;
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mHeatType = "NoUpdate";
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string errors = string.Empty;

            if (validModelNumUpdates() == true)
            {
                objModel.DigitNo = txtModelNoUpdDigitNo.Text;
                objModel.DigitValue = txtModelNoUpdDigitVal.Text;
                objModel.DigitDescription = txtModelNoUpdDigitDesc.Text;
                objModel.DigitRepresentation = txtModelNoUpdDigitRep.Text;
                //objModel.HeatType = "NA";
                objModel.ID = lbModelNoID.Text;

                m_parent.mDigitNo = objModel.DigitNo;
                m_parent.mDigitVal = objModel.DigitValue;
                m_parent.mDigitDesc = objModel.DigitDescription;
                m_parent.mDigitRep = objModel.DigitRepresentation;

                if (rbModelNoHeatTypeNA.Checked == true)
                {
                    objModel.HeatType = "NA";
                }
                else if (rbModelNoHeatTypeGE.Checked == true)
                {
                    objModel.HeatType = "GE";
                }
                else if (rbModelNoHeatTypeElec.Checked == true)
                {
                    objModel.HeatType = "ELEC";
                }

                m_parent.mHeatType = objModel.HeatType;

                if (this.btnSave.Text == "Update")
                {
                    objModel.MSP_UpdateModelNumDesc();
                }
                else if (this.btnSave.Text == "Add")
                {
                    objModel.ID = "";
                    objModel.MSP_InsertModelNumDesc();
                }

                this.Close();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Are you sure you want to do this ModelNumDesc Row? Press 'Yes' to confirm delete & 'No' to cancel!",
                                                      "Deleting a ModelNumDesc Row",
                                                      MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                objModel.ID = this.lbModelNoID.Text;
                objModel.MSP_DeleteModelNumDesc();
                m_parent.mHeatType = "Delete";
                this.Close();
            }
        }

        private bool validModelNumUpdates()
        {
            bool retVal = true;

            string errors = String.Empty;

            if (txtModelNoUpdDigitVal.Text.Length == 0)
            {
                errors += "Error - Digit Val is a required value.\n";
                retVal = false;
            }

            if (txtModelNoUpdDigitDesc.Text.Length == 0)
            {
                errors += "Error - Digit Description is a required value.\n";
            }

            if (txtModelNoUpdDigitRep.Text.Length == 0)
            {
                errors += "Error - Digit Representation is a required value.\n";
            }

            if ((rbModelNoHeatTypeNA.Checked == false) && (rbModelNoHeatTypeElec.Checked == false) &&
                (rbModelNoHeatTypeGE.Checked == false))
            {
                errors += "Error - No Heat Type button has been selected.\n";
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }
    }
}
