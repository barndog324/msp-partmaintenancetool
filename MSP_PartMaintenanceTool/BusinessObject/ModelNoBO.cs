﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSP_PartMaintenanceTool
{
    class ModelNoBO : ModelNoDTO
    {
        //public DataTable MSP_GetCoolingCap()
        //{
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetCoolingCap", connection))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@DigitNo", DigitNo);
        //                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //                {
        //                    if (da.SelectCommand.Connection.State != ConnectionState.Open)
        //                    {
        //                        da.Fill(dt);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return dt;
        //}

        //public DataTable GetVoltageByRevision()
        //{
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetVoltageByRevision", connection))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;

        //                cmd.Parameters.AddWithValue("@DigitNo", DigitNo);
        //                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //                {
        //                    if (da.SelectCommand.Connection.State != ConnectionState.Open)
        //                    {
        //                        da.Fill(dt);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return dt;
        //}

        public DataTable MSP_GetModelNoDigitList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNoDigitList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetModelNoValues()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNoValues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", Int32.Parse(DigitNo));
                        cmd.Parameters.AddWithValue("@HeatType", HeatType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetModelNumDesc()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNumDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", Int32.Parse(DigitNo));
                        cmd.Parameters.AddWithValue("@HeatType", HeatType);
                        cmd.Parameters.AddWithValue("@DigitVal", DigitValue);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void MSP_UpdateModelNumDesc()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.MSP_UpdateModelNumDesc", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@DigitNo", Int32.Parse(DigitNo));
                        command.Parameters.AddWithValue("@DigitVal", DigitValue);
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@DigitDesc", DigitDescription);
                        command.Parameters.AddWithValue("@DigitRep", DigitRepresentation);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MSP_InsertModelNumDesc()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.MSP_InsertModelNumDesc", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@DigitNo", Int32.Parse(DigitNo));
                        command.Parameters.AddWithValue("@DigitVal", DigitValue);
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@DigitDesc", DigitDescription);
                        command.Parameters.AddWithValue("@DigitRep", DigitRepresentation);
                        command.Parameters.Add("@ID", SqlDbType.BigInt);
                        command.Parameters["@ID"].Direction = ParameterDirection.Output;
                        connection.Open();
                        command.ExecuteNonQuery();



                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MSP_DeleteModelNumDesc()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.MSP_DeleteModelNumDesc", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", ID);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DataTable MSP_GetModelNoDigitList()
        //{
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNoDigitList", connection))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //                {
        //                    if (da.SelectCommand.Connection.State != ConnectionState.Open)
        //                    {
        //                        da.Fill(dt);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return dt;
        //}

        public DataTable MSP_GetModelNoList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNoList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ListType", ListType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
