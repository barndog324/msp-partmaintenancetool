﻿namespace MSP_PartMaintenanceTool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPagePartRuleHead = new System.Windows.Forms.TabPage();
            this.btnPRH_Reset = new System.Windows.Forms.Button();
            this.btnPRH_AddPart = new System.Windows.Forms.Button();
            this.txtPRH_SearchPartNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvPartRulesHead = new System.Windows.Forms.DataGridView();
            this.tabPagePartConditions = new System.Windows.Forms.TabPage();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.txtSearchPartNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvPartsMain = new System.Windows.Forms.DataGridView();
            this.tabPageModelNoDesc = new System.Windows.Forms.TabPage();
            this.btnModelNoAddRow = new System.Windows.Forms.Button();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeGE = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeAll = new System.Windows.Forms.RadioButton();
            this.btnModelNoReset = new System.Windows.Forms.Button();
            this.txtSearchModelNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvModelNo = new System.Windows.Forms.DataGridView();
            this.tabPagePartCategory = new System.Windows.Forms.TabPage();
            this.btnPartCatAdd = new System.Windows.Forms.Button();
            this.dgvPartCat = new System.Windows.Forms.DataGridView();
            this.btnMainExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabMain.SuspendLayout();
            this.tabPagePartRuleHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).BeginInit();
            this.tabPagePartConditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).BeginInit();
            this.tabPageModelNoDesc.SuspendLayout();
            this.gbModelNoHeatType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).BeginInit();
            this.tabPagePartCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPagePartRuleHead);
            this.tabMain.Controls.Add(this.tabPagePartConditions);
            this.tabMain.Controls.Add(this.tabPageModelNoDesc);
            this.tabMain.Controls.Add(this.tabPagePartCategory);
            this.tabMain.Location = new System.Drawing.Point(12, 35);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1345, 665);
            this.tabMain.TabIndex = 0;
            // 
            // tabPagePartRuleHead
            // 
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_Reset);
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_AddPart);
            this.tabPagePartRuleHead.Controls.Add(this.txtPRH_SearchPartNum);
            this.tabPagePartRuleHead.Controls.Add(this.label4);
            this.tabPagePartRuleHead.Controls.Add(this.dgvPartRulesHead);
            this.tabPagePartRuleHead.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartRuleHead.Name = "tabPagePartRuleHead";
            this.tabPagePartRuleHead.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePartRuleHead.Size = new System.Drawing.Size(1337, 639);
            this.tabPagePartRuleHead.TabIndex = 0;
            this.tabPagePartRuleHead.Text = "Part Rules Head";
            this.tabPagePartRuleHead.UseVisualStyleBackColor = true;
            // 
            // btnPRH_Reset
            // 
            this.btnPRH_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_Reset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_Reset.Location = new System.Drawing.Point(1066, 7);
            this.btnPRH_Reset.Name = "btnPRH_Reset";
            this.btnPRH_Reset.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_Reset.TabIndex = 14;
            this.btnPRH_Reset.Text = "Reset";
            this.btnPRH_Reset.UseVisualStyleBackColor = true;
            this.btnPRH_Reset.Click += new System.EventHandler(this.btnPRH_Reset_Click);
            // 
            // btnPRH_AddPart
            // 
            this.btnPRH_AddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_AddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_AddPart.Location = new System.Drawing.Point(1187, 7);
            this.btnPRH_AddPart.Name = "btnPRH_AddPart";
            this.btnPRH_AddPart.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_AddPart.TabIndex = 15;
            this.btnPRH_AddPart.Text = "Add Part";
            this.btnPRH_AddPart.UseVisualStyleBackColor = true;
            this.btnPRH_AddPart.Click += new System.EventHandler(this.btnPRH_AddPart_Click);
            // 
            // txtPRH_SearchPartNum
            // 
            this.txtPRH_SearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRH_SearchPartNum.Location = new System.Drawing.Point(603, 9);
            this.txtPRH_SearchPartNum.Name = "txtPRH_SearchPartNum";
            this.txtPRH_SearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtPRH_SearchPartNum.TabIndex = 13;
            this.txtPRH_SearchPartNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPRH_SearchPartNum_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(435, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Search Part Number:";
            // 
            // dgvPartRulesHead
            // 
            this.dgvPartRulesHead.AllowUserToAddRows = false;
            this.dgvPartRulesHead.AllowUserToDeleteRows = false;
            this.dgvPartRulesHead.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvPartRulesHead.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPartRulesHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartRulesHead.Location = new System.Drawing.Point(7, 36);
            this.dgvPartRulesHead.MultiSelect = false;
            this.dgvPartRulesHead.Name = "dgvPartRulesHead";
            this.dgvPartRulesHead.ReadOnly = true;
            this.dgvPartRulesHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartRulesHead.Size = new System.Drawing.Size(1268, 575);
            this.dgvPartRulesHead.TabIndex = 7;
            this.dgvPartRulesHead.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartRulesHead_CellMouseDoubleClick);
            // 
            // tabPagePartConditions
            // 
            this.tabPagePartConditions.Controls.Add(this.btnClear);
            this.tabPagePartConditions.Controls.Add(this.btnReset);
            this.tabPagePartConditions.Controls.Add(this.btnCopy);
            this.tabPagePartConditions.Controls.Add(this.btnAddPart);
            this.tabPagePartConditions.Controls.Add(this.txtSearchPartNum);
            this.tabPagePartConditions.Controls.Add(this.label2);
            this.tabPagePartConditions.Controls.Add(this.dgvPartsMain);
            this.tabPagePartConditions.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartConditions.Name = "tabPagePartConditions";
            this.tabPagePartConditions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePartConditions.Size = new System.Drawing.Size(1337, 639);
            this.tabPagePartConditions.TabIndex = 1;
            this.tabPagePartConditions.Text = "Part Conditions";
            this.tabPagePartConditions.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnClear.Location = new System.Drawing.Point(1014, 13);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear ";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnReset.Location = new System.Drawing.Point(895, 13);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 23);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCopy.Location = new System.Drawing.Point(1239, 13);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(88, 23);
            this.btnCopy.TabIndex = 12;
            this.btnCopy.Text = "Copy ";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnAddPart
            // 
            this.btnAddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnAddPart.Location = new System.Drawing.Point(1127, 13);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(88, 23);
            this.btnAddPart.TabIndex = 11;
            this.btnAddPart.Text = "New Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // txtSearchPartNum
            // 
            this.txtSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchPartNum.Location = new System.Drawing.Point(548, 13);
            this.txtSearchPartNum.Name = "txtSearchPartNum";
            this.txtSearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtSearchPartNum.TabIndex = 9;
            this.txtSearchPartNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchPartNum_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(404, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Search Part Number:";
            // 
            // dgvPartsMain
            // 
            this.dgvPartsMain.AllowUserToAddRows = false;
            this.dgvPartsMain.AllowUserToDeleteRows = false;
            this.dgvPartsMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartsMain.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPartsMain.Location = new System.Drawing.Point(9, 47);
            this.dgvPartsMain.MultiSelect = false;
            this.dgvPartsMain.Name = "dgvPartsMain";
            this.dgvPartsMain.ReadOnly = true;
            this.dgvPartsMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartsMain.Size = new System.Drawing.Size(1318, 575);
            this.dgvPartsMain.TabIndex = 6;
            this.dgvPartsMain.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartsMain_CellMouseDoubleClick);
            this.dgvPartsMain.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPartsMain_DataBindingComplete);
            // 
            // tabPageModelNoDesc
            // 
            this.tabPageModelNoDesc.Controls.Add(this.btnModelNoAddRow);
            this.tabPageModelNoDesc.Controls.Add(this.gbModelNoHeatType);
            this.tabPageModelNoDesc.Controls.Add(this.btnModelNoReset);
            this.tabPageModelNoDesc.Controls.Add(this.txtSearchModelNo);
            this.tabPageModelNoDesc.Controls.Add(this.label3);
            this.tabPageModelNoDesc.Controls.Add(this.dgvModelNo);
            this.tabPageModelNoDesc.Location = new System.Drawing.Point(4, 22);
            this.tabPageModelNoDesc.Name = "tabPageModelNoDesc";
            this.tabPageModelNoDesc.Size = new System.Drawing.Size(1337, 639);
            this.tabPageModelNoDesc.TabIndex = 2;
            this.tabPageModelNoDesc.Text = "ModelNo Desc";
            this.tabPageModelNoDesc.UseVisualStyleBackColor = true;
            // 
            // btnModelNoAddRow
            // 
            this.btnModelNoAddRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoAddRow.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoAddRow.Location = new System.Drawing.Point(584, 48);
            this.btnModelNoAddRow.Name = "btnModelNoAddRow";
            this.btnModelNoAddRow.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoAddRow.TabIndex = 15;
            this.btnModelNoAddRow.Text = "Add Row";
            this.btnModelNoAddRow.UseVisualStyleBackColor = true;
            this.btnModelNoAddRow.Click += new System.EventHandler(this.btnModelNoAddRow_Click);
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeGE);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeAll);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(310, 15);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(183, 61);
            this.gbModelNoHeatType.TabIndex = 14;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 39);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(80, 16);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeGE
            // 
            this.rbModelNoHeatTypeGE.AutoSize = true;
            this.rbModelNoHeatTypeGE.Location = new System.Drawing.Point(80, 39);
            this.rbModelNoHeatTypeGE.Name = "rbModelNoHeatTypeGE";
            this.rbModelNoHeatTypeGE.Size = new System.Drawing.Size(84, 17);
            this.rbModelNoHeatTypeGE.TabIndex = 8;
            this.rbModelNoHeatTypeGE.Text = "Gas/Electric";
            this.rbModelNoHeatTypeGE.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeAll
            // 
            this.rbModelNoHeatTypeAll.AutoSize = true;
            this.rbModelNoHeatTypeAll.Checked = true;
            this.rbModelNoHeatTypeAll.Location = new System.Drawing.Point(7, 16);
            this.rbModelNoHeatTypeAll.Name = "rbModelNoHeatTypeAll";
            this.rbModelNoHeatTypeAll.Size = new System.Drawing.Size(44, 17);
            this.rbModelNoHeatTypeAll.TabIndex = 0;
            this.rbModelNoHeatTypeAll.TabStop = true;
            this.rbModelNoHeatTypeAll.Text = "ALL";
            this.rbModelNoHeatTypeAll.UseVisualStyleBackColor = true;
            // 
            // btnModelNoReset
            // 
            this.btnModelNoReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoReset.Location = new System.Drawing.Point(584, 21);
            this.btnModelNoReset.Name = "btnModelNoReset";
            this.btnModelNoReset.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoReset.TabIndex = 13;
            this.btnModelNoReset.Text = "Reset";
            this.btnModelNoReset.UseVisualStyleBackColor = true;
            this.btnModelNoReset.Click += new System.EventHandler(this.btnModelNoReset_Click);
            // 
            // txtSearchModelNo
            // 
            this.txtSearchModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchModelNo.Location = new System.Drawing.Point(161, 37);
            this.txtSearchModelNo.Name = "txtSearchModelNo";
            this.txtSearchModelNo.Size = new System.Drawing.Size(61, 21);
            this.txtSearchModelNo.TabIndex = 12;
            this.txtSearchModelNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchModelNo_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Search Digit No:";
            // 
            // dgvModelNo
            // 
            this.dgvModelNo.AllowUserToAddRows = false;
            this.dgvModelNo.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.dgvModelNo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvModelNo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvModelNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelNo.Location = new System.Drawing.Point(8, 87);
            this.dgvModelNo.MultiSelect = false;
            this.dgvModelNo.Name = "dgvModelNo";
            this.dgvModelNo.ReadOnly = true;
            this.dgvModelNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelNo.Size = new System.Drawing.Size(674, 525);
            this.dgvModelNo.TabIndex = 10;
            this.dgvModelNo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvModelNo_CellMouseDoubleClick);
            this.dgvModelNo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvModelNo_DataBindingComplete);
            // 
            // tabPagePartCategory
            // 
            this.tabPagePartCategory.Controls.Add(this.btnPartCatAdd);
            this.tabPagePartCategory.Controls.Add(this.dgvPartCat);
            this.tabPagePartCategory.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartCategory.Name = "tabPagePartCategory";
            this.tabPagePartCategory.Size = new System.Drawing.Size(1337, 639);
            this.tabPagePartCategory.TabIndex = 3;
            this.tabPagePartCategory.Text = "Part Category";
            this.tabPagePartCategory.UseVisualStyleBackColor = true;
            // 
            // btnPartCatAdd
            // 
            this.btnPartCatAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartCatAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPartCatAdd.Location = new System.Drawing.Point(708, 11);
            this.btnPartCatAdd.Name = "btnPartCatAdd";
            this.btnPartCatAdd.Size = new System.Drawing.Size(100, 23);
            this.btnPartCatAdd.TabIndex = 44;
            this.btnPartCatAdd.Text = "Add Category";
            this.btnPartCatAdd.UseVisualStyleBackColor = true;
            this.btnPartCatAdd.Click += new System.EventHandler(this.btnPartCatAdd_Click);
            // 
            // dgvPartCat
            // 
            this.dgvPartCat.AllowUserToAddRows = false;
            this.dgvPartCat.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPartCat.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPartCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartCat.Location = new System.Drawing.Point(8, 44);
            this.dgvPartCat.MultiSelect = false;
            this.dgvPartCat.Name = "dgvPartCat";
            this.dgvPartCat.ReadOnly = true;
            this.dgvPartCat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartCat.Size = new System.Drawing.Size(800, 575);
            this.dgvPartCat.TabIndex = 42;
            this.dgvPartCat.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartCat_CellMouseDoubleClick);
            // 
            // btnMainExit
            // 
            this.btnMainExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainExit.ForeColor = System.Drawing.Color.Black;
            this.btnMainExit.Location = new System.Drawing.Point(1262, 12);
            this.btnMainExit.Name = "btnMainExit";
            this.btnMainExit.Size = new System.Drawing.Size(88, 38);
            this.btnMainExit.TabIndex = 3;
            this.btnMainExit.Text = "Exit";
            this.btnMainExit.UseVisualStyleBackColor = true;
            this.btnMainExit.Click += new System.EventHandler(this.btnMainExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(577, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "MSP Part Maintenance Tool";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1369, 692);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMainExit);
            this.Controls.Add(this.tabMain);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MSP Part Maintenance Tool";
            this.tabMain.ResumeLayout(false);
            this.tabPagePartRuleHead.ResumeLayout(false);
            this.tabPagePartRuleHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).EndInit();
            this.tabPagePartConditions.ResumeLayout(false);
            this.tabPagePartConditions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).EndInit();
            this.tabPageModelNoDesc.ResumeLayout(false);
            this.tabPageModelNoDesc.PerformLayout();
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).EndInit();
            this.tabPagePartCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPagePartRuleHead;
        private System.Windows.Forms.TabPage tabPagePartConditions;
        private System.Windows.Forms.TabPage tabPageModelNoDesc;
        private System.Windows.Forms.Button btnPRH_Reset;
        private System.Windows.Forms.Button btnPRH_AddPart;
        private System.Windows.Forms.TextBox txtPRH_SearchPartNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvPartRulesHead;
        private System.Windows.Forms.Button btnMainExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPagePartCategory;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.TextBox txtSearchPartNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvPartsMain;
        private System.Windows.Forms.Button btnModelNoAddRow;
        private System.Windows.Forms.GroupBox gbModelNoHeatType;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeGE;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeAll;
        private System.Windows.Forms.Button btnModelNoReset;
        private System.Windows.Forms.TextBox txtSearchModelNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvModelNo;
        private System.Windows.Forms.Button btnPartCatAdd;
        private System.Windows.Forms.DataGridView dgvPartCat;
    }
}

