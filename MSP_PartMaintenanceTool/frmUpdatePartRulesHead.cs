﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSP_PartMaintenanceTool
{   
    public partial class frmUpdatePartRulesHead : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        private frmMain m_parent;

        public frmUpdatePartRulesHead(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string partNum = cbPRH_PartNum.Text;

            DialogResult result1 = MessageBox.Show("Are you sure you want to do this Part Number: " + partNum + "? Press Yes to delete; No to cancel!",
                                                       "Deleting a PartRulesHead Row",
                                                       MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                objPart.ID = lbID.Text;
                objPart.MSP_DeletePartRulesHead();
                m_parent.mPartCategory = "Delete";
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string mode = "";

            if (btnSave.Text == "Add")
            {
                mode = "Add";
            }
            else
            {
                mode = "Update";
            }

            if (validAddPartRuleHead(mode) == true)
            {
                objPart.PartNum = txtPartNum.Text.ToUpper();
                objPart.PartCategory = cbPRH_PartCategory.Text;
                objPart.RelatedOperation = cbPRH_RelatedOp.Text;
                objPart.PartType = "";
                objPart.AssemblySeq = txtPRH_AsmSeq.Text;

                if (chkBoxPicklist.Checked == true)
                {
                    objPart.Picklist = "Yes";
                }
                else
                {
                    objPart.Picklist = "No";
                }

                objPart.StationLoc = txtPRH_StationLocation.Text;
                objPart.LinesideBin = txtPRH_LinesideBin.Text;
                objPart.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

                if (btnSave.Text == "Add")
                {
                    objPart.MSP_InsertPartRulesHead();
                }
                else
                {
                    objPart.ID = lbID.Text;
                    objPart.MSP_UpdatePartRulesHead();
                }

                m_parent.mPartNumber = objPart.PartNum;
                this.Close();
            }
        }

        private bool validAddPartRuleHead(string mode)
        {
            bool retVal = true;
            string errors = String.Empty;

            if (mode == "Add")
            {
                if (cbPRH_PartNum.SelectedIndex <= 0)
                {
                    errors += "Error - No Part Number has been selected.\n";
                }
                else
                {
                    objPart.PartNum = txtPartNum.Text.ToUpper();

                    DataTable dtPart = objPart.MSP_GetPartRulesHead();

                    if (dtPart.Rows.Count > 0)
                    {
                        errors += "Error - Duplicate Part Number exist.\n";
                    }
                }
            }

            if (cbPRH_PartCategory.SelectedIndex <= 0)
            {
                errors += "Error - No Part Category has been selected.\n";
            }

            if (cbPRH_RelatedOp.SelectedIndex <= 0)
            {
                errors += "Error - No Related Operation has been selected.\n";
            }

            if (txtPRH_AsmSeq.Text.Length == 0)
            {
                errors += "Error - AssemblySeq is a required integer field.\n";
            }
            else
            {
                try
                {
                    int asmSeqInt = Int32.Parse(txtPRH_AsmSeq.Text);
                }
                catch
                {
                    errors += "Error - Invalid data in the AssemblySeq field. AssemblySeq is an integer field.\n";
                }
            }
            
            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private void cbPRH_PartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            string partDescStr = "";

            int dashPos = -1;
            int descLen = 0;

            if (cbPRH_PartNum.SelectedIndex > 0)
            {               
                objPart.PartNum = cbPRH_PartNum.SelectedValue.ToString();
                txtPartNum.Text = objPart.PartNum;
                txtPartNum.Visible = true;
                cbPRH_PartNum.Visible = false;

                dashPos = cbPRH_PartNum.Text.IndexOf("-");
                if (dashPos > 0)
                {
                    ++dashPos;
                }
                descLen = cbPRH_PartNum.Text.Length;
                partDescStr = cbPRH_PartNum.Text.Substring(dashPos, (descLen-dashPos));
                txtPRH_PartDesc.Text = partDescStr;
                //DataTable dtPart = objPart.GetPartDetail();
                //if (dtPart.Rows.Count > 0)
                //{
                //    DataRow drPart = dtPart.Rows[0];
                //    txtPRH_PartDesc.Text = drPart["PartDescription"].ToString();
                //}              
            }        
            else
            {
                txtPRH_PartDesc.Text = "";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtPartNum.Visible = false;
            cbPRH_PartNum.Visible = true;
            txtPRH_PartDesc.Text = "";
            cbPRH_PartCategory.SelectedIndex = 0;
            cbPRH_RelatedOp.SelectedIndex = 0;
            txtPRH_AsmSeq.Text = "";
            txtPRH_StationLocation.Text = "";
            txtPRH_LinesideBin.Text = "";
            chkBoxPicklist.Checked = false;
            cbPRH_PartNum.Focus();
        }
    }
}
